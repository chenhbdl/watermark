package com.chenhb.watermarkutils;

import com.chenhb.watermarkutils.bo.WatermarkBo;
import com.lowagie.text.DocumentException;
import jdk.internal.util.xml.impl.Input;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 水印相关工具
 *
 * @author 陈海波
 * @date 2021-04-21
 */
public class WatermarkUtil {
    /**
     * 给图片添加图片水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toPictureAddImageWatermark(WatermarkBo watermarkBo){
        return ImageImageWatermarkUtil.mark(watermarkBo);
    }

    /**
     * 给图片添加文字水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toPictureAddTextWatermark(WatermarkBo watermarkBo) throws IOException {
        return ImageTextWatermarkUtil.mark(watermarkBo);
    }

    /**
     * 给PDF添加图片水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toPdfAddImageWatermark(WatermarkBo watermarkBo) throws IOException, DocumentException {
        return PdfImageWatermarkUtil.mark(watermarkBo);
    }

    /**
     * 给PDF添加文字水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toPdfAddTextWatermark(WatermarkBo watermarkBo) throws IOException, DocumentException {
        return PdfTextWatermarkUtil.mark(watermarkBo);
    }

    /**
     * 给Word添加图片水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toWordAddImageWatermark(WatermarkBo watermarkBo) throws IOException {
        return WordImageWatermark.mark(watermarkBo);
    }

    /**
     * 给Word添加文字水印
     * @param watermarkBo
     * @return
     */
    public static ByteArrayOutputStream toWordAddTextWatermark(WatermarkBo watermarkBo) throws Exception {
        return WordTextWatermark.mark(watermarkBo);
    }
}
