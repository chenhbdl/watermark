package com.chenhb.watermarkutils;

import java.io.*;
import java.util.Date;

/**
 * 水印抽象类
 *
 * @author chenhb
 * @date 2021-04-21
 */
public abstract class AbstractWatermark {
    /**
     * 将文件转换成byte数组
     *
     * @param file
     * @return
     */
    public static byte[] filetoByte(File file) throws IOException {
        byte[] buffer = null;
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        try {
            fis = new FileInputStream(file);
            bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            fis.close();
            bos.close();
        }
        return buffer;
    }

}
