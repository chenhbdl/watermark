package com.chenhb.watermarkutils.bo;

import lombok.Builder;
import lombok.Data;

import java.awt.*;
import java.io.File;
import java.io.OutputStream;

/**
 * 水印输入对象
 *
 * @author chenhb
 * @date 2021-04-21
 */
@Data
public class WatermarkBo {
    /**
     * 源文件
     */
    private File resourceFile;
    /**
     * 水印图片文件
     */
    private File watermarkImageFile;
    /**
     * 旋转角度
     */
    private Integer degree = 0;
    /**
     * 透明度
     */
    private Float transparency = 1f;
    /**
     * 水印图片宽度
     */
    private Integer width;
    /**
     * 水印图片高度
     */
    private Integer height;
    /**
     * X轴偏移
     */
    private Integer offsetX = 0;
    /**
     * Y轴偏移
     */
    private Integer offsetY = 0;
    /**
     * 文字水印内容
     */
    private String watermarkContent;
    /**
     * 文字大小
     */
    private Integer fontSize = 24;
    /**
     * 字体路径
     */
    private String fontTtf = "C:/Windows/Fonts/simkai.ttf";
    /**
     * 文字水印颜色RGB-R
     */
    private Color color = Color.BLACK;



}
