package com.chenhb.test;


import com.chenhb.watermarkutils.WatermarkUtil;
import com.chenhb.watermarkutils.bo.WatermarkBo;
import com.lowagie.text.DocumentException;

import java.io.*;

public class TestDemo {

    public static void main(String[] args) throws Exception {
//        toPdfAddImageWatermark();
//        toPdfAddTextWatermark();
//        toPictureAddImageWatermark();
//        toPictureAddTextWatermark();
//        toWordAddImageWatermark();
        toWordAddTextWatermark();
    }

    public static void toWordAddTextWatermark() throws Exception {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(50);
        watermarkBo.setWidth(50);
        watermarkBo.setTransparency(0.5f);
        watermarkBo.setFontSize(50);
        File resourceFile = new File("D:/textwatermark.docx");
        watermarkBo.setWatermarkContent("测试水印");
        watermarkBo.setResourceFile(resourceFile);
        byte[] bytes  = WatermarkUtil.toWordAddTextWatermark(watermarkBo).toByteArray();
        FileOutputStream fileOutputStream = new FileOutputStream("D:/word/resultWordText.docx");
        fileOutputStream.write(bytes);
    }

    public static void toWordAddImageWatermark() throws IOException {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(100);
        watermarkBo.setWidth(100);
        watermarkBo.setTransparency(0.5f);
        watermarkBo.setFontSize(50);
        File resourceFile = new File("D:/textwatermark.docx");
        watermarkBo.setWatermarkContent("测试水印");
        watermarkBo.setResourceFile(resourceFile);
        File imageFile = new File("D:/resource.jpg");
        watermarkBo.setWatermarkImageFile(imageFile);
        ByteArrayOutputStream  bytes = WatermarkUtil.toWordAddImageWatermark(watermarkBo);
        FileOutputStream fileOutputStream = new FileOutputStream("D:/word/resultWordImage.docx");
        fileOutputStream.write(bytes.toByteArray());
    }

    public static void toPdfAddTextWatermark() throws IOException, DocumentException {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(100);
        watermarkBo.setWidth(100);
        watermarkBo.setTransparency(0.5f);
        watermarkBo.setFontSize(50);
        File resourceFile = new File("D:/ReferenceCard.pdf");
        watermarkBo.setWatermarkContent("测试水印");
        watermarkBo.setResourceFile(resourceFile);
        byte[] bytes   = WatermarkUtil.toPdfAddTextWatermark(watermarkBo).toByteArray();
        FileOutputStream fileOutputStream = new FileOutputStream("D:/pdf/resultPdfText.pdf");
        fileOutputStream.write(bytes);
    }

    public static void toPdfAddImageWatermark() throws IOException, DocumentException {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(100);
        watermarkBo.setWidth(100);
        watermarkBo.setTransparency(0.5f);
        watermarkBo.setFontSize(50);
        File resourceFile = new File("D:/ReferenceCard.pdf");
        watermarkBo.setWatermarkContent("测试水印");
        watermarkBo.setResourceFile(resourceFile);
        File imageFile = new File("D:/resource.jpg");
        watermarkBo.setWatermarkImageFile(imageFile);
        byte[] bytes = WatermarkUtil.toPdfAddImageWatermark(watermarkBo).toByteArray();
        FileOutputStream fileOutputStream = new FileOutputStream("D:/pdf/resultPdf.pdf");
        fileOutputStream.write(bytes);
//        write(fileOutputStream, inputStream);
    }

    public static void toPictureAddTextWatermark() throws IOException {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(100);
        watermarkBo.setWidth(100);
        watermarkBo.setTransparency(0.5f);
        watermarkBo.setFontSize(50);
        File resourceFile = new File("D:/test.jpg");
        watermarkBo.setWatermarkContent("测试水印");
        watermarkBo.setResourceFile(resourceFile);
        byte[] bytes = WatermarkUtil.toPictureAddTextWatermark(watermarkBo).toByteArray();
        FileOutputStream fileOutputStream = new FileOutputStream("D:/img/resultText.jpg");
        fileOutputStream.write(bytes);
    }

    public static void toPictureAddImageWatermark() throws IOException {
        WatermarkBo watermarkBo = new WatermarkBo();
        watermarkBo.setDegree(25);
        watermarkBo.setOffsetX(100);
        watermarkBo.setOffsetY(100);
        watermarkBo.setHeight(100);
        watermarkBo.setWidth(100);
        watermarkBo.setTransparency(0.5f);
        File resourceFile = new File("D:/test.jpg");
        File imageFile = new File("D:/resource.jpg");
        watermarkBo.setWatermarkImageFile(imageFile);
        watermarkBo.setResourceFile(resourceFile);
        byte[] bytes = WatermarkUtil.toPictureAddImageWatermark(watermarkBo).toByteArray();
        FileOutputStream fileOutputStream = new FileOutputStream("D:/img/result.jpg");
        fileOutputStream.write(bytes);
    }


}
